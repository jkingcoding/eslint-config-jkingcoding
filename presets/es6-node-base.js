module.exports = {
  extends: [
    './rules/best-practices',
    './rules/errors',
    './rules/es6',
    './rules/imports'
    './rules/node',
    './rules/style',
    './rules/strict',
    './rules/variables'
  ].map(require.resolve),
  rules: {
      indent: [
          2,
          4, {
              SwitchCase: 1,
              VariableDeclarator: 1
          }
      ]
  }
  env: {
    jasmine: 'true'
  }
};
